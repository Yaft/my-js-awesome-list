> my awesome js link list

## [Vue.js](https://vuejs.org)

* [Style Guide](https://ru.vuejs.org/v2/style-guide/) - This is the official style guide for Vue-specific code
* [tutorial](http://www.vuejsradar.com/building-vuejs-dropdown-component/?utm_campaign=VueJS%2BRadar&utm_medium=email&utm_source=VueJS_Radar_32) - Build a Vue.js Drop Down Profile Menu component
* [tutorial](https://alligator.io/vuejs/functional-components/?utm_campaign=VueJS%2BRadar&utm_medium=email&utm_source=VueJS_Radar_32) - Functional Components in Vue.js
* [tutorial](https://blog.sqreen.io/authentication-best-practices-vue/?utm_campaign=VueJS%2BRadar&utm_medium=email&utm_source=VueJS_Radar_32) - Authentication Best Practices for Vue
* [Dynamic-Components](https://vuejs.org/v2/guide/components.html#Dynamic-Components) - Динамическое переключение компонентов
* [tutorial](https://vuejsdevelopers.com/2018/01/08/vue-js-roles-permissions-casl/) - Managing User Permissions
* [Render Prop](https://vuejsdevelopers.com/2018/01/15/vue-js-render-props/) - I show that Vue can use JSX to utilize the React pattern of a render prop
* [ES2015 Features](https://vuejsdevelopers.com/2018/01/22/vue-js-javascript-es6/) - 4 Essential ES2015 Features For Vue.js Development
* [Chart.js](https://alligator.io/vuejs/vue-chart-js/?utm_campaign=VueJS%2BRadar&utm_medium=web&utm_source=VueJS_Radar_33) - Using Chart.js with Vue.js
* [surface shader](https://github.com/grzhan/vue-flat-surface-shader?ref=madewithvuejs.com) - A Vue component for flat surface shader
* [charts](https://www.chartkick.com/vue) - charts mfk!
* [component](https://github.com/anteriovieira/vue-mindmap?ref=madewithvuejs.com) - mindmap

## [vuex](https://vuex.vuejs.org/ru/)
> _паттерн управления состоянием и библиотека для приложений на Vue.js_

* [mutation](https://vuex.vuejs.org/ru/mutations.html) - Мутации
* [tutorial](https://vuejsdevelopers.com/2017/11/13/vue-js-vuex-undo-redo/) - Create A Vuex Undo/Redo Plugin For VueJS

## [nuxt.js](https://nuxtjs.org/)

 * [modules](https://nuxtjs.org/guide/modules/) - About nuxt module system
 * [plugins](https://nuxtjs.org/guide/plugins) - 

## JavaScript

* [es6 classes](http://jsraccoon.ru/es6-classes) - Классы в es6
* [MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript) - Introduction to Object-Oriented JavaScript
* [gitbooks](https://karmazzin.gitbooks.io/eloquentjavascript_ru/) - Eloquent Javascript
* [Object Explorer](https://sdras.github.io/object-explorer/) - Find the object method you need without digging through the docs
* [Array Explorer](https://sdras.github.io/array-explorer/) - Find the array method you need without digging through the docs

- - - -

## Other

 * [gmusic](https://unofficial-google-music-api.readthedocs.io/en/latest/reference/protocol.html#module-gmusicapi.protocol.musicmanager) - unofficial google music api
  * [lib api](https://github.com/jamon/playmusic) - Node-JS Google Play Music API 
  * [issue](https://github.com/jamon/playmusic/issues/47) - `getPlayListEntries` has limit of 1000 records, allow filtering by playlist id
  * [issue](https://github.com/simon-weber/gmusicapi/issues/175) - `get_all_playlist_contents()` doesn't populate tracks in each playlist

 * [webogram](https://github.com/zhukov/webogram) - webogram source code
 * [emoji-data](https://github.com/iamcal/emoji-data) - Easy to parse data and spritesheets for emoji 
 * [cloudy ui](https://www.figma.com/file/DlOf4Er5DaurwGtG0FNbUjkO?embed_host=share&node-id=48%3A551&viewer=1) - cloudy ui prototype

 * [svg to png](https://habrahabr.ru/post/254973/) - Конвертируем svg to png
 * [MDN](https://developer.mozilla.org/ru/docs/Web/API/Canvas_API/Drawing_DOM_objects_into_a_canvas) - dom to canvas
 
 * [AssemblyScript](https://github.com/AssemblyScript/assemblyscript/wiki/Status-and-Roadmap) - A TypeScript to WebAssembly compiler

 - - - -

## non-js

 * [Latte-Dock](https://github.com/psifidotos/Latte-Dock) - Latte is a dock based on plasma frameworks that provides an elegant and intuitive experience for your tasks and plasmoids.
 * [Unity Layout ](https://store.kde.org/p/1206059/) - A Unity layout for Latte Dock>=0.7 that is using the Latte task manager... 